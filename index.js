const GC_THROTTLE = 1000 // 1 Garbage Collector call per 1000 release cycle

export class LockManager {
	constructor(options)
	{
		options = typeof options === 'object' && options ? options : {};
		this.defaults = {
			concurrency: options.concurrency || -1, // number of semaphore lock available simultanously per key
			timeout: options.timeout || -1, // time before rejected lock acquisition
			ttl: options.ttl || -1, // time before automatic lock release
		};
		this.semaphores = {}
		this.locks = {}
		this.__GC_COUNT = 0

		this.addSemaphore('*');
	}
	addSemaphore(semaphore,options=null) {
		options = typeof options === 'object' && options ? options : this.defaults;
		this.semaphores[semaphore] = {
			concurrency: options.concurrency || -1, // number of semaphore lock available simultanously per key
			timeout: options.timeout || -1, // time before rejected lock acquisition
			ttl: options.ttl || -1, // time before automatic lock release
		};
	}

	getLocks(lock) {
		return this.locks[lock.key] || new Set()
	}
	addLock(lock) {
		if(!this.locks[lock.key])
			this.locks[lock.key] = new Set();
		this.locks[lock.key].add(lock)
	}
	deleteLock(lock)
	{
		if(this.locks[lock.key])
		{
			this.locks[lock.key].delete(lock)
		}
		this.__gc();
	}

	__gc() {
		this.__GC_COUNT++
		if(this.__GC_COUNT>=GC_THROTTLE)
		{
			console.log('GC');
			this.__GC_COUNT = 0;
			for(let key in this.locks)
			{
				if(this.locks[key].size===0)
					delete this.locks[key]
			}
		}
	}

	acquire(...params) {
		let semaphore = '*';
		let key = '*';

		switch(params.length) {
			case 0: throw 'LockManager.request need at least one parameter'; break;
			case 1: key = params[0]; break;
			case 2: semaphore = params[0]; key = params[1]; break;
		}

		let lock = new Lock(this,semaphore,key)
		this.addLock(lock);

		if(this.canAcquire(lock))
			lock.__resolve()
		else if(this.semaphores[lock.semaphore].timeout>0) {
			setTimeout(lock.__reject.bind(lock),this.semaphores[lock.semaphore].timeout)
		}

		return lock.promise;
	}

	release(lock) {
		this.deleteLock(lock)
		let locks = Array.from(this.getLocks(lock));
		for(let i=0; i<locks.length; i++)
		{
			let lock = locks[i]
			if(this.canAcquire(lock))
				lock.__resolve()
		}
	}

	canAcquire(lock) {
		if(!this.semaphores[lock.semaphore])
			return false;

		let mixed = false
		let concurrency = 0;
		let locks = Array.from(this.getLocks(lock));
		for(let i=0; i<locks.length; i++)
		{
			if(lock === locks[i])
				break;

			if(lock.semaphore === locks[i].semaphore)
				concurrency++;
			else
				mixed = true;

			if(mixed || concurrency >= this.semaphores[lock.semaphore].concurrency)
				return false;
		}
		//console.log(lock.semaphore,lock.i,concurrency);
		return true;
	}

	async request(...params) {
		let semaphore = '*';
		let key = '*';
		let callback = null;

		switch(params.length) {
			case 0: throw 'LockManager.request need at least one parameter'; break;
			case 1: callback = params[0]; break;
			case 2: key = params[0]; callback = params[1]; break;
			case 3: semaphore = params[0]; key = params[1]; callback = params[2]; break;
		}

		if(!typeof callback === 'function')
			throw `LockManager.request callback must be a function. received ${typeof callback} instead`;

		try {
			let l = await this.acquire(semaphore,key);
			let r = await callback()
			l.release();
			return r;
		}
		catch(e)
		{
			console.log(`LockManager.Request(${semaphore},${key},callback...) failed`)
			return false;
		}
	}
}

export class Lock {
	static i=0;

	constructor(manager,semaphore,key) {
		this.i = this.constructor.i++;
		this.status = null;
		this.manager = manager
		this.semaphore = semaphore
		this.key = key

		this.timer_ttl = null;

		this.promise = new Promise((resolve, reject) => {
			this.promiseResolve = resolve
			this.promiseReject = reject
		})
	}
	__reject() {
		if(this.status===null)
		{
			console.log(`Lock ${this.semaphore}::${this.key} Rejected`)
			this.status = false;
			this.release()
			this.promiseReject(this)

		}
	}
	__resolve() {
		if(this.status===null)
		{
			this.status = true;
			this.promiseResolve(this)
			if(this.manager.semaphores[this.semaphore].ttl>0) {
				this.timer_ttl = setTimeout(() => {
					this.release()
					console.log(`Lock ${this.semaphore}::${this.key} Expired`)
				},this.manager.semaphores[this.semaphore].ttl)
			}
		}
	}
	release() {
		clearTimeout(this.timer_ttl)
		this.manager.release(this)
	}
}

export default LockManager

/*
// **********************************************************
// Usage
// **********************************************************
import wait from 'wait'

let fsLock = new RessourceLockManager()
fsLock.addSemaphore('read',{concurrency:10,timeout:2000,ttl:2000})
fsLock.addSemaphore('write',{concurrency:1,timeout:2000,ttl:2000})

async function read(key) {
	try {
		let l = await fsLock.acquire('read',key);
		console.log(`read ${key}`)
		await wait(1000);
		console.log(`read ${key} done`)
		l.release();
	}
	catch(e)
	{
		console.log(`read ${key} failed`)
	}
}
async function write(key) {
	try {
		let l = await fsLock.acquire('write',key);
		console.log(`write ${key}`)
		await wait(5000);
		console.log(`write ${key} done`)
		l.release();
	}
	catch(e)
	{
		console.log(`write ${key} failed`)
	}
}

read('file 1');
read('file 1');
read('file 2');
read('file 1');
read('file 2');
read('file 3');
write('file 1');
read('file 1');
read('file 2');
read('file 3');
read('file 1');
read('file 2');
read('file 3');

console.log('Tasks in queue');
*/